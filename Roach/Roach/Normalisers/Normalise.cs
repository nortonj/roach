﻿using Roach.DataModel;
using System;
using System.Collections.Generic;

namespace Roach.Normalisers
{
    public enum AggregateType { Avg, Min, Max }

    public static class Normalise
    {
        public static IEnumerable<Item> ToModel(this IEnumerable<Item> target, IEnumerable<Item> modeledOn, AggregateType aggregateType, TimeSpan? offset = null)
        { 
            if (modeledOn == null || target == null) throw new ArgumentNullException();

            if (offset == null) offset = new TimeSpan();

            var eModeledOn = modeledOn.GetEnumerator();
            var eTarget = target.GetEnumerator();
            eTarget.MoveNext();
            eModeledOn.MoveNext();
            var prevKey = eModeledOn.Current.Key.Add(offset.Value);
           
            var completed = false;
            var modeledOnEnd = false;

            while (!completed)
            {
                if (!eModeledOn.MoveNext()) modeledOnEnd = true;
                var count = 0;
                var sum = 0.0;
                var max = double.MinValue;
                var min = double.MaxValue;

                while(!completed && (modeledOnEnd || eTarget.Current.Key >= eModeledOn.Current.Key))
                {
                    if (eTarget.Current.Key <= prevKey)
                    {
                        count++;
                        sum += eTarget.Current.Value;
                        if (eTarget.Current.Value > max) max = eTarget.Current.Value;
                        if (eTarget.Current.Value < min) min = eTarget.Current.Value;
                    }
                    if (!eTarget.MoveNext()) completed = true;
                }

                var val = 0.0;
                if (aggregateType == AggregateType.Avg) val = sum / count;
                else if (aggregateType == AggregateType.Min) val = min;
                else if (aggregateType == AggregateType.Max) val = max;

                yield return new Item(prevKey, val);

                prevKey = eModeledOn.Current.Key.Add(offset.Value);
            }
        }

        public static IEnumerable<Item> ToChange(this IEnumerable<Item> target)
        {
            var eTarget = target.GetEnumerator();
            eTarget.MoveNext();

            var completed = false;
            var prev = eTarget.Current;

            while (!completed)
            {
                if (!eTarget.MoveNext())
                {
                    completed = true;
                }
                else
                {
                    var change = new Item(prev.Key, prev.Value - eTarget.Current.Value);
                    prev = eTarget.Current;
                    yield return change;
                }
            }
        }

    }
}
