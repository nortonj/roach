﻿using Roach.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Roach.Normalisers
{
    public class Transform
    {
        public IEnumerable<Item> Result { get; private set; }
        public IEnumerable<Item> Target { get; private set; }

        public double Shift { get; private set; }
        public double Factor { get; private set; }

        public double Max { get; private set; }
        public double Min { get; private set; }
        public double Range { get; private set; }
  
        public double Lower { get; private set; }
        public double Upper { get; private set; }

        public Transform(IEnumerable<Item> target, double upper, double lower)
        {
            if (target == null) throw new ArgumentNullException();
            Target = target;
            Lower = lower;
            Upper = upper;
            
            Initialize();
        }

        private void Initialize()
        {
            Max = Target.Select(kvp=>kvp.Value).Max();
            Min = Target.Select(kvp => kvp.Value).Min();
            Range = Max - Min;
            Factor = (Upper - Lower) / Range;
            Shift = Upper - Max * Factor;
            Result = Target.Select(t => new Item(t.Key, (t.Value * Factor) + Shift));
        }
    } 
}
