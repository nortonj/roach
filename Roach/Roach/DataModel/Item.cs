﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roach.DataModel
{
    public class Item 
    {
        public DateTime Key { get; set; }
        public double Value { get; set; }

        public Item(DateTime key, double value)
        {
            Key = key;
            Value = value;
        }
    }
}

