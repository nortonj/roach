﻿using Roach.Converters;
using Roach.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Roach.Extensions
{
    public static class StringParserExtensions
    {
        public static string[][] ToJaggedArray(this string str, char rowDelimiter = ';', char colDelimiter = ',')
        {
            if (string.IsNullOrEmpty(str))
                throw new ArgumentException("string is null or empty.");
            return str.Split(rowDelimiter).Select(i => i.Split(colDelimiter).ToArray()).ToArray();
        }

        public static IEnumerable<Item> ToTimeseries(this string str, char rowDelimiter = ';', char colDelimiter = ',')
        {
            var array = str.ToJaggedArray(rowDelimiter, colDelimiter);
            foreach(var row in array)
            {
                var key = TypeConverters.ChangeType<DateTime>(row[0]);
                var value = TypeConverters.ChangeType<double>(row[1]);
                yield return new Item(key, value);
            }
        }
    }
}
