﻿using Roach.DataModel;
using Roach.Normalisers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Roach.Extensions
{
    public static class EnumerableExtensions
    {
        public static string TimeseriesToString(this IEnumerable<Item> value, char rowDelimiter = ';', char colDelimiter = ',') 
        {
            if (value == null)
                throw new ArgumentException("dictionary is null.");
            if (value.Count() < 1)
                return string.Empty;
            var str = new StringBuilder();
            foreach (var keyValuePair in value)
                str.Append($"{keyValuePair.Key.ToString()}{colDelimiter}{keyValuePair.Value.ToString()}{rowDelimiter}");
            str.Remove(str.Length - 1, 1);
            return str.ToString();
        }

        public static IEnumerable<T>TakePercent<T>(this IEnumerable<T> target, double percent)
        {
            return target.Take((int)(target.Count() * percent / 100));
        }

        public static IEnumerable<T>SkipPercent<T>(this IEnumerable<T> target, double percent)
        {
            return target.Skip((int)(target.Count() * percent / 100));
        }

        public static Transform ToTransform(this IEnumerable<Item> target, double upper, double lower)
        {
            return new Transform(target, upper, lower);
        }

        public static double[][] ZipToJaggedArray(this IEnumerable<Item> source, IEnumerable<Item> target)
        {
            return source.Zip(target, (x, y) => new[] { x.Value, y.Value }).ToArray();
        }

        public static double[][] ToJaggedArray(this IEnumerable<Item> target)
        {
            return target.Select(x => new[] { x.Value}).ToArray();
        }
    }
}
