﻿using Encog.Engine.Network.Activation;
using Encog.ML.Data.Basic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation;
using Encog.Neural.Networks.Training.Propagation.Resilient;
using System.Collections.Generic;

namespace Roach.Encog
{
    public class BasicMLService
    {
        private BasicNetwork _basicNetwork;
        private Propagation _propogation;

        public BasicMLService(BasicNetwork basicNetwork)
        {
            _basicNetwork = basicNetwork;
        }

        public int Train(double[][] explain, double[][] expect, double error = 0.001, int maxIterations = 1000)
        {
            var mlDataSet = new BasicMLDataSet(explain, expect);
            var train = new ResilientPropagation(_basicNetwork, mlDataSet);

            int iteration = 1;
            do
            {
                train.Iteration();
                //System.Diagnostics.Debug.WriteLine("Iteration No :{0}, Error: {1}", iteration, train.Error);
            } while (iteration++ <= maxIterations && train.Error > error);

            return iteration;

            //var train = new Backpropagation(basicNetwork, trainingSet 0.7, 0.2);
            //var train = new ManhattanPropagation(basicNetwork, trainingSet, 0.01);
            //var train = new ScaledConjugateGradient(basicNetwork, trainingSet);
            //var train = new LevenbergMarquardtTraining(basicNetwork, trainingSet);
            //var train = new QuickPropagation(basicNetwork, trainingSet, 2.0);
        }

        public double[][] Evaluate(double[][] explain, double[][] expect, double shift = 0, double factor = 1)
        {
            var mlDataSet = new BasicMLDataSet(explain, expect);
            var res = new List<double[]>();
            foreach (var item in mlDataSet)
            {
                var output = _basicNetwork.Compute(item.Input);
                System.Diagnostics.Debug.WriteLine("Input:{0},{1} Targer:{2} Modeled:{3}", item.Input[0], item.Input[1], (item.Ideal[0] - shift)/ factor, (output[0] - shift)/ factor);
                res.Add(new double[] { (item.Ideal[0] - shift) / factor, (output[0] - shift) / factor });
            }

            return res.ToArray();
        }

     }
}
