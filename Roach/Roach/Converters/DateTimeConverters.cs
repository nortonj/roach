﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roach.Converters
{
    public enum Seasons {Spring = 1, Summer, Autumn, Winter}

    public static class DateTimeConverters
    {
        public static Seasons ToSeason(this DateTime date)
        {
            int doy = date.DayOfYear - Convert.ToInt32((DateTime.IsLeapYear(date.Year)) && date.DayOfYear > 59);

            if (doy < 80 || doy >= 355) return Seasons.Winter;

            if (doy >= 80 && doy < 172) return Seasons.Spring;

            if (doy >= 172 && doy < 266) return Seasons.Summer;

            return Seasons.Autumn;
        }

        public static double ToQuater(this DateTime date)
        {
            if (date.Month < 4) return 1;

            if (date.Month < 7) return 2;

            if (date.Month < 10) return 3;

            return 4;
        }
    }
}
