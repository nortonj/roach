﻿using System;
using System.ComponentModel;

namespace Roach.Converters
{
    public static class TypeConverters
    {
        public static T ChangeType<T>(object value)
        {
            return (T)ChangeType(typeof(T), value);
        }

        public static object ChangeType(Type t, object value)
        {
            TypeConverter tc = TypeDescriptor.GetConverter(t);
            return tc.ConvertFrom(value);
        }

        public static void RegisterTypeConverter<T1, T2>() where T2 : TypeConverter
        {
            TypeDescriptor.AddAttributes(typeof(T1), new TypeConverterAttribute(typeof(T2)));
        }
    }
}
