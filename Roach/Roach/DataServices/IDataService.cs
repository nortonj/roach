﻿using Roach.DataModel;
using System;
using System.Collections.Generic;

namespace Roach.DataServices
{
    public interface IDataService
    {
        IEnumerable<Item> Subscribe();
    }
}