﻿using Roach.DataModel;
using Roach.Extensions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Roach.DataServices
{
    public enum QuandlOrder { Asc, Desc }

    public class QuandlDataService : IDataService
    {
        private string _url = @"https://www.quandl.com/api/v3/datasets/[database]/[ticker]/data.csv?column_index=[columnIndex]&exclude_column_names=true";
        private string _apiKey = "JaGyEBTEfMe6MNYu742g";
        private string _dateFormat = "yyyy-MM-dd";

        private string _database, _ticker;
        private QuandlOrder _order;
        private DateTime? _startDate, _endDate;
        private int _columnIndex;

        public QuandlDataService(string database, string ticker, int columnIndex = 4, QuandlOrder order = QuandlOrder.Desc): this(database, ticker, null, null, columnIndex, order)
        {

        }

        public QuandlDataService(string database, string ticker, DateTime? startDate, DateTime? endDate, int columnIndex = 4, QuandlOrder order = QuandlOrder.Desc)
        {
            _database = database;
            _ticker = ticker;
            _startDate = startDate;
            _endDate = endDate;
            _columnIndex = columnIndex;
            _order = order;
        }

        public IEnumerable<Item> Subscribe()
        {
            var url = ParseUrlString();
            using (var quandl = new WebClient())
            {
                var r = quandl.DownloadString(url);
                var res = r.ToJaggedArray('\n');

                for (var row = 0; row < res.GetUpperBound(0); row++)
                {
                    DateTime dateTime;
                    if (!DateTime.TryParse(res[row][0], out dateTime)) throw new FormatException("returned none datetime value.");

                    double val;
                    if (!double.TryParse(res[row][1], out val)) throw new FormatException("returned none numeric value.");
                    
                    yield return new Item(dateTime, val);
                }
            }
        }

        private string ParseUrlString()
        {
            var url = new StringBuilder(_url);

            url.Replace("[database]", _database)
              .Replace("[ticker]", _ticker)
              .Replace("[columnIndex]", _columnIndex.ToString());

            if (_startDate.HasValue) url.Append("&start_date=[startDate]".Replace("[startDate]", _startDate.Value.ToString(_dateFormat)));
            if (_endDate.HasValue) url.Append("&end_date=[endDate]".Replace("[endDate]", _endDate.Value.ToString(_dateFormat)));

            url.Append("&order=[order]".Replace("[order]", _order.ToString()));
            url.Append("&api_key=[apiKey]".Replace("[apiKey]", _apiKey));

            return url.ToString();
        }
    }
}
