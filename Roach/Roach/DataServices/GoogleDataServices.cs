﻿using System.Net;
using System;
using System.Collections.Generic;
using Roach.Extensions;
using Roach.DataModel;

namespace Roach.DataServices
{
    public class GoogleDataService : IDataService
    {
        private string _url = @"http://www.google.com/finance/getprices?i=[period]&p=[days]d&amp;f=d,o,h,l,c,v&df=cpct&q=[ticker]";
        private string _ticker, _exchange, _column;
        private int _period, _days;

        public GoogleDataService(string ticker, string column, int period, int days)
        {
            _ticker = ticker;
            _column = column;
            _period = period;
            _days = days;
        }

        public IEnumerable<Item> Subscribe()
        {
            var url = ParseUrlString();

            using (var google = new WebClient())
            {
                var r = google.DownloadString(url);
                var res = r.ToJaggedArray('\n');
                if (res.GetUpperBound(0) < 7) throw new FormatException("Nothing returned check parameters.");

                var col = Array.FindIndex(res[4], c => c == _column);
                if (col < 0) throw new FormatException("Invalid Column.");

                _exchange = res[0][0].Split('%')[1];

                int openMin;
                if (!int.TryParse(res[1][0].Split('=')[1], out openMin)) throw new FormatException("Open interval not numeric.");

                int interval;
                if (!int.TryParse(res[3][0].Split('=')[1], out interval)) throw new FormatException("Open interval not numeric.");

                var startDateTime = DateTime.MinValue;

                for (var row = 7; row < res.GetUpperBound(0); row++)
                {
                    var dateTime = DateTime.MinValue;
                    if (res[row][0].StartsWith("a"))
                        dateTime = startDateTime = DateTime.FromOADate(int.Parse(res[row][0].TrimStart('a')) / 86400 + 25569 + (5.5 / 24)).AddMinutes(openMin);
                    else
                        dateTime = startDateTime.AddSeconds(int.Parse(res[row][0]) * interval);

                    double val;
                    if (!double.TryParse(res[row][col], out val)) throw new FormatException("returned none numeric value.");

                    yield return new Item(dateTime, val);
                }
            }
        }

        private string ParseUrlString()
        {
            return _url.Replace("[ticker]", _ticker)
                 .Replace("[period]", _period.ToString())
                 .Replace("[days]", _days.ToString());
        }
    }
}
