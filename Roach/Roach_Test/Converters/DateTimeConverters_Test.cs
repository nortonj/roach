﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roach.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roach_Test.Converters
{
    [TestClass]
    public class DateTimeConverters_Test
    {
        [TestMethod]
        public void ToSeason_RangeDateTimes()
        {
            Assert.AreEqual(Seasons.Spring, new DateTime(15, 3, 21).ToSeason());
            Assert.AreEqual(Seasons.Spring, new DateTime(15, 6, 20).ToSeason());

            Assert.AreEqual(Seasons.Summer, new DateTime(15, 6, 21).ToSeason());
            Assert.AreEqual(Seasons.Summer, new DateTime(15, 9, 22).ToSeason());

            Assert.AreEqual(Seasons.Autumn, new DateTime(15, 9, 23).ToSeason());
            Assert.AreEqual(Seasons.Autumn, new DateTime(15, 12, 20).ToSeason());

            Assert.AreEqual(Seasons.Winter, new DateTime(15, 12, 21).ToSeason());
            Assert.AreEqual(Seasons.Winter, new DateTime(16, 3, 20).ToSeason());
        }

        [TestMethod]
        public void ToQuater_RangeDateTimes()
        {
            Assert.AreEqual(1, new DateTime(16, 1, 21).ToQuater());
            Assert.AreEqual(1, new DateTime(16, 3, 31).ToQuater());

            Assert.AreEqual(2, new DateTime(16, 4, 1).ToQuater());
            Assert.AreEqual(2, new DateTime(16, 6, 30).ToQuater());

            Assert.AreEqual(3, new DateTime(16, 7, 1).ToQuater());
            Assert.AreEqual(3, new DateTime(16, 9, 30).ToQuater());

            Assert.AreEqual(4, new DateTime(16, 10, 1).ToQuater());
            Assert.AreEqual(4, new DateTime(16, 12, 31).ToQuater());
        }
    }
}
