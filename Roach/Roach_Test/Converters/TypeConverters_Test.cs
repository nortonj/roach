﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roach.Converters;
using System;

namespace Roach_Test.Converters
{
    [TestClass]
    public class TypeConverters_Test
    {
        [TestMethod]
        public void ChangeType_DateTime()
        {
            var dateTime = new DateTime(16, 11, 3);
            var dateTimeFromString = TypeConverters.ChangeType<DateTime>(dateTime.ToString());

            Assert.AreEqual(dateTime, dateTimeFromString);
        }

        [TestMethod]
        public void ChangeType_Double()
        {
            var dbl = 3.4;
            var doubleFromString = TypeConverters.ChangeType<double>(dbl.ToString());

            Assert.AreEqual(dbl, doubleFromString);
        }
    }
}
