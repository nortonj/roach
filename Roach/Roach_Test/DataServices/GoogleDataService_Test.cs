﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Roach.DataServices;
using Roach.Extensions;
using System.Linq;

namespace Roach_Test.DataService
{
    [TestClass]
    public class GoogleDataService_Test
    {
        
        [TestMethod]
        [Ignore]
        //example to moq data service for other test
        public void MockExample()
        {
            var data = @"24/10/2016 15:00:00,150.57;25/10/2016 15:00:00,150.88;26/10/2016 15:00:00,151.81;27/10/2016 15:00:00,153.35;28/10/2016 15:00:00,152.61;31/10/2016 15:00:00,153.69;01/11/2016 15:00:00,152.79;02/11/2016 15:00:00,151.95";
            var enumerable = data.ToTimeseries();
            var dataServiceMock = new Mock<IDataService>();
            dataServiceMock.SetupGet(x => x.Subscribe()).Returns(enumerable);

            Assert.AreEqual(enumerable, dataServiceMock.Object.Subscribe());
        }

        [TestMethod]
        [Ignore]
        //can connect
        public void CanConnect()
        {
            var dataService = new GoogleDataService("IBM","CLOSE", 0,20);
            var count = dataService.Subscribe().Count();
            Assert.IsTrue(count > 5);
        }
    }
}