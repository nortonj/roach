﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Roach.DataServices;
using Roach.Extensions;
using System;
using System.Linq;

namespace Roach_Test.DataService
{
    [TestClass]
    public class QuandlDataService_Test
    {
        [TestMethod]
        [Ignore]
        //example to moq data service for other test
        public void MockExample()
        {
            var data = @"26/11/2013 00:00:00,45.89;27/11/2013 00:00:00,46.49;29/11/2013 00:00:00,47.01";
            var enumerable = data.ToTimeseries();
            var dataServiceMock = new Mock<IDataService>();
            dataServiceMock.SetupGet(x => x.Subscribe()).Returns(enumerable);

            Assert.Equals(enumerable.TimeseriesToString(), dataServiceMock.Object.Subscribe().TimeseriesToString());
        }

        [TestMethod]
        //[Ignore]
        //can connect
        public void CanConnect()
        {
            var start = new DateTime(2012, 11, 01);
            var end = new DateTime(2013, 11, 30);
            var dataService = new QuandlDataService("WIKI", "FB", start, end);
            
            Assert.IsTrue(dataService.Subscribe().Count() > 5);
        }
    }
}