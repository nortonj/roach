﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roach.DataModel;
using Roach.Extensions;
using Roach.Normalisers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Roach_Test.Normalisers
{
    [TestClass]
    public class Normalise_Test
    {
        [TestMethod]
        public void ModelOn_UnmatchedSetsAvg()
        {
            var modeledOnString = "01/10/2016,144952;01/09/2016,144791;01/08/2016,144600;01/07/2016,144424;01/06/2016,144172";

            var targetString = "29/10/2016,265000;22/10/2016,258000;15/10/2016,261000;08/10/2016,247000;" +
                               "01/10/2016,246000;24/09/2016,254000;17/09/2016,251000;10/09/2016,260000;03/09/2016,259000;" +
                               "27/08/2016,263000;20/08/2016,261000;13/08/2016,262000;06/08/2016,266000;" +
                               "30/07/2016,267000;23/07/2016,266000;16/07/2016,252000;09/07/2016,254000;02/07/2016,254000;" +
                               "25/06/2016,270000;18/06/2016,258000;11/06/2016,277000;04/06/2016,264000;" +
                               "28/05/2016,268000;21/05/2016,268000;14/05/2016,278000;07/05/2016,294000";

            var expected = "01/10/2016 00:00:00,254000;01/09/2016 00:00:00,263000;01/08/2016 00:00:00,258600;01/07/2016 00:00:00,267250;01/06/2016 00:00:00,277000";

            var modeledOn = modeledOnString.ToTimeseries();
            var target = targetString.ToTimeseries();
            var result = target.ToModel(modeledOn, AggregateType.Avg).TimeseriesToString();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ModelOn_UnmatchedSetsMax()
        {
            var modeledOnString = "01/10/2016,144952;01/09/2016,144791;01/08/2016,144600;01/07/2016,144424;01/06/2016,144172";

            var targetString = "29/10/2016,265000;22/10/2016,258000;15/10/2016,261000;08/10/2016,247000;" +
                               "01/10/2016,246000;24/09/2016,254000;17/09/2016,251000;10/09/2016,260000;03/09/2016,259000;" +
                               "27/08/2016,263000;20/08/2016,261000;13/08/2016,262000;06/08/2016,266000;"+
                               "30/07/2016,267000;23/07/2016,266000;16/07/2016,252000;09/07/2016,254000;02/07/2016,254000;"+
                               "25/06/2016,270000;18/06/2016,258000;11/06/2016,277000;04/06/2016,264000;"+
                               "28/05/2016,268000;21/05/2016,268000;14/05/2016,278000;07/05/2016,294000";

            var expected = "01/10/2016 00:00:00,260000;01/09/2016 00:00:00,266000;01/08/2016 00:00:00,267000;01/07/2016 00:00:00,277000;01/06/2016 00:00:00,294000";

            var modeledOn = modeledOnString.ToTimeseries();
            var target = targetString.ToTimeseries();
            var result = target.ToModel(modeledOn, AggregateType.Max).TimeseriesToString();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ModelOn_UnmatchedSetsMin()
        {
            var modeledOnString = "01/10/2016,144952;01/09/2016,144791;01/08/2016,144600;01/07/2016,144424;01/06/2016,144172";

            var targetString = "29/10/2016,265000;22/10/2016,258000;15/10/2016,261000;08/10/2016,247000;" +
                               "01/10/2016,246000;24/09/2016,254000;17/09/2016,251000;10/09/2016,260000;03/09/2016,259000;" +
                               "27/08/2016,263000;20/08/2016,261000;13/08/2016,262000;06/08/2016,266000;" +
                               "30/07/2016,267000;23/07/2016,266000;16/07/2016,252000;09/07/2016,254000;02/07/2016,254000;" +
                               "25/06/2016,270000;18/06/2016,258000;11/06/2016,277000;04/06/2016,264000;" +
                               "28/05/2016,268000;21/05/2016,268000;14/05/2016,278000;07/05/2016,294000";

            var expected = "01/10/2016 00:00:00,246000;01/09/2016 00:00:00,261000;01/08/2016 00:00:00,252000;01/07/2016 00:00:00,258000;01/06/2016 00:00:00,268000";

            var modeledOn = modeledOnString.ToTimeseries();
            var target = targetString.ToTimeseries();
            var result = target.ToModel(modeledOn, AggregateType.Min).TimeseriesToString();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ModelOn_NullTargetException()
        {
            Normalise.ToModel(null, new List<Item>(), AggregateType.Avg).ToList();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ModelOn_NullModeledOnException()
        {
            new List<Item>().ToModel(null, AggregateType.Avg).ToList();
        }
    }
}
