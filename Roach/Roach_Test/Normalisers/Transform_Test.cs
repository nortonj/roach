﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roach.Extensions;
using Roach.Normalisers;
using System;
using System.Linq;

namespace Roach_Test.Normalisers
{
    [TestClass]
    public class Transform_Test
    {
        [TestMethod]
        public void Transform_FiveValuesSigmoid()
        {
            var data = "01/10/2016 00:00:00,246000;01/09/2016 00:00:00,261000;01/08/2016 00:00:00,252000;01/07/2016 00:00:00,258000;01/06/2016 00:00:00,268000";
            var expected = "01/10/2016 00:00:00,-1;01/09/2016 00:00:00,0.363636363636363;01/08/2016 00:00:00,-0.454545454545457;01/07/2016 00:00:00,0.0909090909090899;01/06/2016 00:00:00,1";
                          
            var modeledOn = data.ToTimeseries();

            var transform = new Transform(modeledOn, 1.0, -1.0);
            var inverse = new Transform(transform.Result, transform.Max, transform.Min);

            Assert.AreEqual(transform.Result.TimeseriesToString(), expected);
            Assert.AreEqual(inverse.Result.TimeseriesToString(), data);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ModeledOn_NullTargetException()
        {
            var transform = new Transform(null, 1F, -1F);
        }
    }
}
