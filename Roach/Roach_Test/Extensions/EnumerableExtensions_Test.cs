﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roach.DataModel;
using Roach.Extensions;
using System;
using System.Collections.Generic;

namespace Roach_Test.Extensions
{
    [TestClass]
    public class EnumerableExtensions_Test
    {
        [TestMethod]
        public void ToDelimitedString_KeysDateTimedoubleValues()
        {
            var timeseries = new List<Item>();
            timeseries.Add(new Item(new DateTime(16, 11, 3), 1.1));
            timeseries.Add(new Item(new DateTime(16, 11, 4), 2.3));
            timeseries.Add(new Item(new DateTime(16, 11, 5), 4.5));
            var str = timeseries.TimeseriesToString();

            Assert.AreEqual(str, "03/11/0016 00:00:00,1.1;04/11/0016 00:00:00,2.3;05/11/0016 00:00:00,4.5");
        }

        [TestMethod]
        public void ToDelimitedString_EmptyDictionary()
        {
            var timeseries = new List<Item>();
            var str = timeseries.TimeseriesToString();

            Assert.IsTrue(str==string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ToDelimitedString_NullException()
        {
            EnumerableExtensions.TimeseriesToString(null);
        }
    }
}
