﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Roach.Extensions;
using System.Linq;

namespace Roach_Test.Extensions
{
    [TestClass]
    public class StringParserExtensions_Test
    {
        [TestMethod]
        public void ToJaggedArray_ThreeOddArrays()
        {
            var str = "1,A;2,B,C;3,E";
            var strArray = str.ToJaggedArray();

            Assert.AreEqual(strArray.Length, 3);
            Assert.AreEqual(strArray[0].Length, 2);
            Assert.AreEqual(strArray[1].Length, 3);
            Assert.AreEqual(strArray[2].Length, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ToJaggedArray_EmptyStringException()
        {
            var str = string.Empty;
            var strArray = str.ToJaggedArray();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ToJaggedArray_NullStringException()
        {
            string str = null;
            var strArray = StringParserExtensions.ToJaggedArray(str);
        }

        [TestMethod]
        public void ToDictionary_EightKeysDateTimedoubleValues()
        {
            var str = @"24/10/2016 15:00:00,150.57;25/10/2016 15:00:00,150.88;26/10/2016 15:00:00,151.81;27/10/2016 15:00:00,153.35;28/10/2016 15:00:00,152.61;31/10/2016 15:00:00,153.69;01/11/2016 15:00:00,152.79;02/11/2016 15:00:00,151.95";
            var enumerable = str.ToTimeseries();

            Assert.AreEqual(enumerable.Count(), 8);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ToDictionary_NullStringException()
        {
            var str = string.Empty;
            var dict = str.ToTimeseries().ToList();
        }
    }
}
