﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RoachView.Startup))]
namespace RoachView
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
