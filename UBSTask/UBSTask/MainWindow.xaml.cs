﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UBSTask.ViewModel;

namespace UBSTask
{
    [Export(typeof(MainWindow))]
    public partial class MainWindow : Window
    {

        [ImportingConstructor]
        public MainWindow([Import] IFundViewModel viewModel,
            [Import("TradeInputControlTemplate")] ControlTemplate tradeInputControlTemplate,
            [Import("FundControlTemplate")] ControlTemplate fundControlTemplate,
            [Import("FundSummaryControlTemplate")] ControlTemplate FundSummaryControlTemplate)
        {
            InitializeComponent();

            DataContext = viewModel;
            TradeInputContentControl.Template = tradeInputControlTemplate;
            FundContentControl.Template = fundControlTemplate;
            FundTotalContentControl.Template = FundSummaryControlTemplate;
        }
    }
}
