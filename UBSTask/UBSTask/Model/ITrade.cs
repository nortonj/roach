﻿using System;

namespace UBSTask.Model
{
    public interface ITrade: IEquatable<ITrade>
    {
        IStock Stock { get; }
        float MarketValue { get; }
        float Price { get; }
        float Quantity { get; }
        float TransactionCost { get; }
    }
}