﻿using System;

namespace UBSTask.Model
{
    public class Stock : IStock
    {
        public string Name { get; private set; }
        public StockType StockType { get; private set; }
        public float TransactionFee { get; private set; }
        public float Tolerance { get; private set; }

        public Stock(StockType stockType, string name, float transactionFee, float tolerance)
        {
            Name = name;
            StockType = stockType;
            TransactionFee = transactionFee;
            Tolerance = tolerance;
        }

        public bool Equals(IStock other)
        {
            if (Name == other.Name && StockType == other.StockType && TransactionFee == other.TransactionFee)
                return true;
            return false;
        }
    }
}
