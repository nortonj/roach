﻿using System.ComponentModel;

namespace UBSTask.Model
{
    public enum StockType
    {
        [Description("Equity Description")]
        Equity,
        [Description("Bond Description")]
        Bond
    }
}
