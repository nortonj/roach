﻿using System;

namespace UBSTask.Model
{
    public interface IStock: IEquatable<IStock>
    {
        StockType StockType { get; }
        string Name { get; }
        float TransactionFee { get; }
        float Tolerance { get; }
    }
}