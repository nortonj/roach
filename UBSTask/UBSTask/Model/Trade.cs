﻿using System;

namespace UBSTask.Model
{
    public class Trade : ITrade, IEquatable<ITrade>
    {
        public IStock Stock { get; private set; }
        public float Price { get; private set; }
        public float Quantity { get; private set; }
        public float MarketValue { get { return Price * Quantity ; } }
        public float TransactionCost { get { return Math.Abs(MarketValue * Stock.TransactionFee); } }

        public Trade(IStock stock, float price, float quantity)
        {
            Stock = stock;
            Price = price;
            Quantity = quantity;
        }

        public bool Equals(ITrade other)
        {
            if (Stock.Equals(other.Stock) && Price == other.Price && Quantity == other.Quantity)
                return true;
            return false;
        }
    }
}
