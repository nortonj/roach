﻿using System.Collections.ObjectModel;
using UBSTask.Model;

namespace UBSTask.ViewModel
{
    public interface IFundViewModel
    {
        ObservableCollection<ITrade> Fund { get; }
        ICaptureViewModel Capture { get; }

        void AddTrade();
    }
}