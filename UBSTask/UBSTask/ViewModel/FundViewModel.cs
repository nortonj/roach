﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Input;
using UBSTask.Commands;
using UBSTask.Model;

namespace UBSTask.ViewModel
{
    [Export(typeof(IFundViewModel))]
    public class FundViewModel : ViewModelBase, INotifyPropertyChanged, IFundViewModel
    {
        public ObservableCollection<ITrade> Fund { get; private set; } = new ObservableCollection<ITrade>();
        public ICommand AddToFundCommand { get; private set; }
        public ICaptureViewModel Capture { get; private set; }

        [ImportingConstructor]
        public FundViewModel([Import]ICaptureViewModel capture)
        {
            Capture = capture;
            AddToFundCommand = new DelegateCommand(OnAddToFundExecute, OnAddToFundCanExecute);
        }

        public void AddTrade()
        {
            var trade = Capture.GetTrade();
            Capture.FundSummary.AddTrade(trade);
            OnPropertyChanged("InputStockName");

            Fund.Add(trade);
        }

        private bool OnAddToFundCanExecute(object obj)
        {
            return true;
        }

        private void OnAddToFundExecute(object obj)
        {
            AddTrade();
        }
    }
}
