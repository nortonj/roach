﻿namespace UBSTask.ViewModel
{
    public interface IFundTotalViewModel
    {
        double Number { get; set; }
        double MarketValue { get; set; }
        double StockWeight { get; set; }
    }
}