﻿using System.ComponentModel.Composition;
using UBSTask.Model;

namespace UBSTask.ViewModel
{
    [Export(typeof(IFundSummaryViewModel))]
    public class FundSummaryViewModel : IFundSummaryViewModel
    {
        public IFundTotalViewModel EquityFundTotal { get; private set; }
        public IFundTotalViewModel BondFundTotal { get; private set; }
        public IFundTotalViewModel AllFundTotal { get; private set; }

        [ImportingConstructor]
        public FundSummaryViewModel([Import]IFundTotalViewModel equityFundTotal,
                                    [Import]IFundTotalViewModel bondFundTotal,
                                    [Import]IFundTotalViewModel allFundTotal)
        {
            EquityFundTotal = equityFundTotal;
            BondFundTotal = bondFundTotal;
            AllFundTotal = allFundTotal;
        }

        public void AddTrade(ITrade trade)
        {
            AllFundTotal.Number++;
            AllFundTotal.MarketValue += trade.MarketValue;
            if (AllFundTotal.StockWeight == 0)
                AllFundTotal.StockWeight = 100;

            if (trade.Stock.StockType == StockType.Equity)
            {
                EquityFundTotal.Number++;
                EquityFundTotal.MarketValue += trade.MarketValue;
            }
            else
            {
                BondFundTotal.Number++;
                BondFundTotal.MarketValue += trade.MarketValue;
            }

            EquityFundTotal.StockWeight = EquityFundTotal.MarketValue / AllFundTotal.MarketValue * 100;
            BondFundTotal.StockWeight = BondFundTotal.MarketValue / AllFundTotal.MarketValue * 100;
        }
    }
}
