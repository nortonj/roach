﻿using System.ComponentModel.Composition;

namespace UBSTask.ViewModel
{
    [Export(typeof(IFundTotalViewModel)) PartCreationPolicy(CreationPolicy.NonShared)]
    public class FundTotalViewModel : ViewModelBase, IFundTotalViewModel
    {
        public double Number { get { return _number; } set { _number = value; OnPropertyChanged(); } }
        public double StockWeight { get { return _stockWeight; } set { _stockWeight = value; OnPropertyChanged(); } }
        public double MarketValue { get { return _marketValue; } set { _marketValue = value; OnPropertyChanged(); } }
        private double _number, _stockWeight, _marketValue;
    }
}
