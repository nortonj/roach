﻿using UBSTask.Model;

namespace UBSTask.ViewModel
{
    public interface ICaptureViewModel
    {
        StockType StockType { get; set; }
        IFundSummaryViewModel FundSummary { get; }
        float Price { get; set; }
        float Quantity { get; set; }
        string StockName { get; }

        IStock GetStock();
        ITrade GetTrade();
    }
}