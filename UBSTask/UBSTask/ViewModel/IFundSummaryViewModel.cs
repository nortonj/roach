﻿using UBSTask.Model;

namespace UBSTask.ViewModel
{
    public interface IFundSummaryViewModel
    {
        IFundTotalViewModel AllFundTotal { get; }
        IFundTotalViewModel BondFundTotal { get; }
        IFundTotalViewModel EquityFundTotal { get; }
        void AddTrade(ITrade trade);
    }
}