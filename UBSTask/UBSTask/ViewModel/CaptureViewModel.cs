﻿using System.ComponentModel.Composition;
using UBSTask.Model;

namespace UBSTask.ViewModel
{
    [Export(typeof(ICaptureViewModel))]
    public class CaptureViewModel : ViewModelBase, ICaptureViewModel
    {
        public IFundSummaryViewModel FundSummary { get; private set; }

        public StockType StockType { get { return _inputStockType; } set { _inputStockType = value; OnPropertyChanged(); OnPropertyChanged("InputStockName"); } }
        public string StockName
        {
            get
            {
                return StockType == StockType.Equity ? $"{StockType}{FundSummary.EquityFundTotal.Number + 1}" : $"{StockType}{FundSummary.BondFundTotal.Number + 1}";
            }
        }
        public float Quantity { get { return _quantity; } set { _quantity = value; OnPropertyChanged(); } }
        public float Price { get { return _price; } set { _price = value; OnPropertyChanged(); } }

        private float _price = 100, _quantity = 100;
        private StockType _inputStockType = StockType.Equity;

        [ImportingConstructor]
        public CaptureViewModel([Import]IFundSummaryViewModel fundSummary)
        {
            FundSummary = fundSummary;
        }

        public IStock GetStock()
        {
            if (StockType == StockType.Equity)
            { 
                return new Stock(StockType, $"{StockType}{FundSummary.EquityFundTotal.Number + 1}", 0.02F, 200000);
            }
            else
            {
                return new Stock(StockType, $"{StockType}{FundSummary.BondFundTotal.Number + 1}", 0.005F, 100000);
            }
        }

        public ITrade GetTrade()
        {
            return new Trade(GetStock(), Price, Quantity);
        }
    }
}

