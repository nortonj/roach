﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace UBSTask.StartUp
{
    public class BootStrapper
    {
        public MainWindow Bootstrap()
        {
            var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            var container = new CompositionContainer(catalog);

            var tradeInputControlTemplate = Application.Current.Resources["TradeInputControlTemplate"] as ControlTemplate;
            if (tradeInputControlTemplate == null) throw new ResourceReferenceKeyNotFoundException();
            container.ComposeExportedValue("TradeInputControlTemplate", tradeInputControlTemplate);

            var fundControlTemplate = Application.Current.Resources["FundControlTemplate"] as ControlTemplate;
            if (fundControlTemplate == null) throw new ResourceReferenceKeyNotFoundException();
            container.ComposeExportedValue("FundControlTemplate", fundControlTemplate);

            var FundSummaryControlTemplate = Application.Current.Resources["FundSummaryControlTemplate"] as ControlTemplate;
            if (FundSummaryControlTemplate == null) throw new ResourceReferenceKeyNotFoundException();
            container.ComposeExportedValue("FundSummaryControlTemplate", FundSummaryControlTemplate);

            var mainWindow = container.GetExportedValue<MainWindow>();
            return mainWindow;
        }
    }
}
