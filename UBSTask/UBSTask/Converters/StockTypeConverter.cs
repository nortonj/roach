﻿using System;
using System.Windows.Data;
using UBSTask.Model;

namespace UBSTask.Converters
{
    public class StockTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (StockType)Enum.Parse(typeof(StockType), value.ToString(), true);
        }
    }
}
