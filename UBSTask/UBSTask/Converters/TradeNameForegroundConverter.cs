﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using UBSTask.Model;

namespace UBSTask.Converters
{
    public class TradeNameForegroundConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var stock = (IStock)values[0];
            var marketValue = (float)values[1];
            var transactionCost = (float)values[2];
            
            if(marketValue < 0)
                return Brushes.Red;

            if(transactionCost > stock.Tolerance)
                return Brushes.Red;

            return Brushes.Black;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

