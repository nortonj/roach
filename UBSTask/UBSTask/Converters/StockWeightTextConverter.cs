﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace UBSTask.Converters
{
    public class StockWeightTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var marketValue = (float)values[0];
            var allMarketValue = (double)values[1];
            return (marketValue / allMarketValue).ToString("#.00%");
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

