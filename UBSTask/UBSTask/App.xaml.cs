﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using UBSTask.StartUp;

namespace UBSTask
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var bootstrapper = new BootStrapper();
            var mainWindow = bootstrapper.Bootstrap();
            mainWindow.Show();
        }
    }
}
