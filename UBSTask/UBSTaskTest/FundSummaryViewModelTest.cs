﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UBSTask.Model;
using UBSTask.ViewModel;

namespace UBSTaskTest
{
    [TestClass]
    public class FundSummaryTotalViewModelTest
    {
        private static Mock<ITrade> bondTrade, equityTrade;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var bondStock = new Mock<IStock>();
            bondStock.SetupGet(x => x.StockType).Returns(StockType.Bond);
            bondStock.SetupGet(x => x.TransactionFee).Returns(0.1F);

            bondTrade = new Mock<ITrade>();
            bondTrade.SetupGet(x => x.Stock).Returns(bondStock.Object);
            bondTrade.SetupGet(x => x.Price).Returns(2F);
            bondTrade.SetupGet(x => x.Quantity).Returns(2F);
            bondTrade.SetupGet(x => x.MarketValue).Returns(bondTrade.Object.Quantity * bondTrade.Object.Price);

            var equityStock = new Mock<IStock>();
            equityStock.SetupGet(x => x.StockType).Returns(StockType.Equity);
            equityStock.SetupGet(x => x.TransactionFee).Returns(0.3F);

            equityTrade = new Mock<ITrade>();
            equityTrade.SetupGet(x => x.Stock).Returns(equityStock.Object);
            equityTrade.SetupGet(x => x.Price).Returns(3F);
            equityTrade.SetupGet(x => x.Quantity).Returns(3F);
            equityTrade.SetupGet(x => x.MarketValue).Returns(equityTrade.Object.Quantity * equityTrade.Object.Price);
        }

        [TestMethod]
        public void AddOneBond()
        {
            var sut = GetNewSut();

            sut.AddTrade(bondTrade.Object);

            AssertNumbers(sut, 1, 1, 0);
            AssertMarketValues(sut, 4, 4, 0);
            AssertStockWeights(sut, 100, 100, 0);
        }

        [TestMethod]
        public void AddOneEquity()
        {
            var sut = GetNewSut();

            sut.AddTrade(equityTrade.Object);

            AssertNumbers(sut, 1, 0, 1);
            AssertMarketValues(sut, 9, 0, 9);
            AssertStockWeights(sut, 100, 0, 100);
        }

        [TestMethod]
        public void AddOneBondAndOneEquity()
        {
            var sut = GetNewSut();

            sut.AddTrade(bondTrade.Object);
            sut.AddTrade(equityTrade.Object);

            AssertNumbers(sut, 2, 1, 1);
            AssertMarketValues(sut, 13, 4, 9);
            AssertStockWeights(sut, 100, (float)4/13*100, (float)9/13*100);
        }

        [TestMethod]
        public void AddTwoBondsAndOneEquity()
        {
            var sut = GetNewSut();

            sut.AddTrade(bondTrade.Object);
            sut.AddTrade(bondTrade.Object);
            sut.AddTrade(equityTrade.Object);

            AssertNumbers(sut, 3, 2, 1);
            AssertMarketValues(sut, 17, 8, 9);
            AssertStockWeights(sut, 100, (float)8/17*100, (float)9/17*100);
        }

        [TestMethod]
        public void AddOneBondAndTwoEquitys()
        {
            var sut = GetNewSut();

            sut.AddTrade(bondTrade.Object);
            sut.AddTrade(equityTrade.Object);
            sut.AddTrade(equityTrade.Object);

            AssertNumbers(sut, 3, 1, 2);
            AssertMarketValues(sut, 22, 4, 18);
            AssertStockWeights(sut, 100, (float)4/22*100, (float)18/22*100);
        }

        private void AssertNumbers(IFundSummaryViewModel sut, float all, float bond, float equity)
        {         
            Assert.AreEqual(sut.AllFundTotal.Number, all, 0.0001);
            Assert.AreEqual(sut.BondFundTotal.Number, bond, 0.0001);
            Assert.AreEqual(sut.EquityFundTotal.Number, equity, 0.0001);
        }

        private void AssertMarketValues(IFundSummaryViewModel sut, float all, float bond, float equity)
        {
            Assert.AreEqual(sut.AllFundTotal.MarketValue, all, 0.0001);
            Assert.AreEqual(sut.BondFundTotal.MarketValue, bond, 0.0001);
            Assert.AreEqual(sut.EquityFundTotal.MarketValue, equity, 0.0001);
        }

        private void AssertStockWeights(IFundSummaryViewModel sut, float all, float bond, float equity)
        {
            Assert.AreEqual(sut.AllFundTotal.StockWeight, all, 0.0001);
            Assert.AreEqual(sut.BondFundTotal.StockWeight, bond, 0.0001);
            Assert.AreEqual(sut.EquityFundTotal.StockWeight, equity, 0.0001);
        }

        private IFundSummaryViewModel GetNewSut()
        {
            var eft = new Mock<IFundTotalViewModel>();
            var bft = new Mock<IFundTotalViewModel>();
            var aft = new Mock<IFundTotalViewModel>();

            eft.SetupAllProperties();
            bft.SetupAllProperties();
            aft.SetupAllProperties();

            return new FundSummaryViewModel(eft.Object, bft.Object, aft.Object);
        }
    }
}
