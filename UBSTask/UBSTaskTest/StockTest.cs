﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBSTask.Model;

namespace UBSTaskTest
{
    [TestClass]
    public class StockTest
    {
        [TestMethod]
        public void CheckPropertySetsOnConstruction()
        {
            var stock = new Stock(StockType.Equity,"Equity1", 0.05F, 10000);

            Assert.AreEqual(stock.StockType, StockType.Equity);
            Assert.AreEqual(stock.Name, "Equity1");
            Assert.AreEqual(stock.TransactionFee, 0.05F);
        }
    }
}
