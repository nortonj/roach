﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBSTask.ViewModel;

namespace UBSTaskTest
{
    [TestClass]
    public class FundTotalViewModelTest
    {
        [TestMethod]
        public void Number_SetAndNotifyPropertyChanged()
        {
            var sut = new FundTotalViewModel();
            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) => 
            {
                if (e.PropertyName == "Number")
                    eventPropertyChanged = true;
            };

            sut.Number = 20;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.Number, 20);
        }

        public void MarketValue_SetAndNotifyPropertyChanged()
        {
            var sut = new FundTotalViewModel();

            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "MarketValue")
                    eventPropertyChanged = true;
            };

            sut.MarketValue = 20;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.MarketValue, 20);
        }

        public void StockWeight_SetAndNotifyPropertyChanged()
        {
            var sut = new FundTotalViewModel();
            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "StockWeight")
                    eventPropertyChanged = true;
            };

            sut.StockWeight = 20;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.StockWeight, 20);
        }
    }
}
