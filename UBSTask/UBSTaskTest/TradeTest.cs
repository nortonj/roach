﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UBSTask.Model;
using Moq;

namespace UBSTaskTest
{
    [TestClass]
    public class TradeTest
    {
        private static Mock<IStock> bondStock, equityStock;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            bondStock = new Mock<IStock>();
            bondStock.SetupGet(x => x.TransactionFee).Returns(0.02F);
            
            equityStock = new Mock<IStock>();
            equityStock.SetupGet(x => x.TransactionFee).Returns(0.005F);
        }

        [TestMethod]
        public void CheckPropertySetsOnConstruction()
        {
            var trade = new Trade(bondStock.Object, 101F, 2000F);

            Assert.AreEqual(trade.Stock, bondStock.Object);
            Assert.AreEqual(trade.Price, 101F);
            Assert.AreEqual(trade.Quantity, 2000F);
        }

        [TestMethod]
        public void BondMarketValueTest()
        {
            var trade = new Trade(bondStock.Object, 101, 40);

            Assert.AreEqual(trade.MarketValue, 4040, 0.00001);
        }

        [TestMethod]
        public void EquityMarketValueTest()
        {
            var trade = new Trade(equityStock.Object, 50, 20);

            Assert.AreEqual(trade.MarketValue, 1000, 0.00001);
        }

        [TestMethod]
        public void BondTransactionCostTest()
        {
            var trade = new Trade(bondStock.Object, 101, 40);

            Assert.AreEqual(trade.TransactionCost, 4040*0.02, 0.00001);
        }

        [TestMethod]
        public void EquityTransactionCostTest()
        {
            var trade = new Trade(equityStock.Object, 50, 20);

            Assert.AreEqual(trade.TransactionCost, 1000*0.005, 0.00001);
        }
    }
}
