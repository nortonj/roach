﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UBSTask.Model;
using UBSTask.ViewModel;

namespace UBSTaskTest
{
    [TestClass]
    public class CaptureViewModelTest
    {
        [TestMethod]
        public void GetOneEquity()
        {
            var sut = GetNewSut();

            sut.Quantity = 2;
            sut.Price = 3;
            sut.StockType = StockType.Equity;

            var t = sut.GetTrade();

            Assert.AreEqual(t.Quantity, 2);
            Assert.AreEqual(t.Price, 3);
            Assert.AreEqual(t.Stock.StockType, StockType.Equity);
            Assert.AreEqual(t.Stock.Name, "Equity1");
        }
        [TestMethod]
        public void GetOneBond()
        {
            var sut = GetNewSut();
            sut.Quantity = 2;
            sut.Price = 3;
            sut.StockType = StockType.Bond;

            var t = sut.GetTrade();

            Assert.AreEqual(t.Quantity, 2);
            Assert.AreEqual(t.Price, 3);
            Assert.AreEqual(t.Stock.StockType, StockType.Bond);
            Assert.AreEqual(t.Stock.Name, "Bond1");
        }

        public void CheckStockNameTwoEquitiesOneBond()
        {
            var sut = GetNewSut();

            sut.Quantity = 2;
            sut.Price = 3;

            sut.StockType = StockType.Equity;
            var e1 = sut.GetTrade();
          
            sut.StockType = StockType.Bond;
            var b1 = sut.GetTrade();

            sut.StockType = StockType.Equity;
            var e2 = sut.GetTrade();

            Assert.AreEqual(e1.Stock.Name, "Equity1");
            Assert.AreEqual(e2.Stock.Name, "Equity2");
            Assert.AreEqual(b1.Stock.Name, "Bond1");
        }

        [TestMethod]
        public void Quantity_SetAndNotifyPropertyChanged()
        {
            var fs = new Mock<IFundSummaryViewModel>();
            var sut = new CaptureViewModel(fs.Object);

            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Quantity")
                    eventPropertyChanged = true;
            };
            sut.Quantity = 20;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.Quantity, 20);
        }

        [TestMethod]
        public void Price_SetAndNotifyPropertyChanged()
        {
            var fs = new Mock<IFundSummaryViewModel>();
            var sut = new CaptureViewModel(fs.Object);

            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "Price")
                    eventPropertyChanged = true;
            };
            sut.Price = 20;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.Price, 20);
        }

        [TestMethod]
        public void StockType_SetAndNotifyPropertyChanged()
        {
            var fs = new Mock<IFundSummaryViewModel>();
            var sut = new CaptureViewModel(fs.Object);

            bool eventPropertyChanged = false;
            sut.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName == "StockType")
                    eventPropertyChanged = true;
            };
            sut.StockType = StockType.Equity;

            Assert.IsTrue(eventPropertyChanged);
            Assert.AreEqual(sut.StockType, StockType.Equity);
        }

        private ICaptureViewModel GetNewSut()
        {
            var eft = new Mock<IFundTotalViewModel>();
            var bft = new Mock<IFundTotalViewModel>();
            var aft = new Mock<IFundTotalViewModel>();
            eft.SetupAllProperties();
            bft.SetupAllProperties();
            aft.SetupAllProperties();

            var fs = new Mock<IFundSummaryViewModel>();
            fs.SetupGet(x => x.EquityFundTotal).Returns(eft.Object);
            fs.SetupGet(x => x.BondFundTotal).Returns(bft.Object);
            fs.SetupGet(x => x.AllFundTotal).Returns(aft.Object);

            return new CaptureViewModel(fs.Object);
        }
    }
}
