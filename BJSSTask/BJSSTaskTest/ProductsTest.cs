﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BJSSTaskCore;
using Moq;

namespace BJSSTaskTest
{
    [TestClass]
    public class ProductsTest
    {
        [TestMethod]
        public void CheckAdd_OneProduct()
        {
            var product = new Mock<IProduct>();
            product.Setup(x => x.Name).Returns("Product1");

            var sut = new Products();
            sut.Add(product.Object);

            Assert.AreEqual(sut.List.Count, 1);
        }

        [TestMethod]
        public void CheckAdd_TwoProducts()
        {

            var product1 = new Mock<IProduct>();
            product1.Setup(x => x.Name).Returns("Product1");
            var product2 = new Mock<IProduct>();
            product2.Setup(x => x.Name).Returns("Product2");

            var sut = new Products();
            sut.Add(product1.Object);
            sut.Add(product2.Object);

            Assert.AreEqual(sut.List.Count, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Product1 already in products")]
        public void CheckAdd_OneProductTwice()
        {
            var product = new Mock<IProduct>();
            product.Setup(x => x.Name).Returns("Product1");

            var sut = new Products();
            sut.Add(product.Object);
            sut.Add(product.Object);
        }
    }
}
