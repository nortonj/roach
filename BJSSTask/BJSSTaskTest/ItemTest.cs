﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using BJSSTaskCore;

namespace BJSSTaskTest
{
    [TestClass]
    public class ItemTest
    {
        [TestMethod]
        public void PropertiesSet_OnConstruction()
        {
            var prod = new Mock<IProduct>();
            prod.SetupGet(x => x.Name).Returns("Product1");
            var sut = new Item(prod.Object, 3);

            Assert.AreEqual(sut.Product, prod.Object);
            Assert.AreEqual(sut.Quantity, 3);
        }

        [TestMethod]
        public void CheckCost()
        {
            var name = "Product1";
            var price = 10.0F;
            var discount = 0.01F;
            var prod = new Mock<IProduct>();
            prod.SetupGet(x => x.Name).Returns(name);
            prod.SetupGet(x => x.Price).Returns(price);
            prod.SetupGet(x => x.Discount).Returns(discount);

            var sut = new Item(prod.Object, 2);

            Assert.AreEqual(sut.Cost, 2 * 10, 0.0001);
        }

        [TestMethod]
        public void CheckDiscount_WithTenPercent()
        {
            var name = "Product1";
            var price = 10.0F;
            var discount = 0.01F;
            var prod = new Mock<IProduct>();
            prod.SetupGet(x => x.Name).Returns(name);
            prod.SetupGet(x => x.Price).Returns(price);
            prod.SetupGet(x => x.Discount).Returns(discount);

            var sut = new Item(prod.Object, 2);

            Assert.AreEqual(sut.Discount, 2 * 10 * 0.01F, 0.0001);
        }

        [TestMethod]
        public void CheckDiscount_WhenZero()
        {
            var name = "Product1";
            var price = 10.0F;
            var discount = 0;
            var prod = new Mock<IProduct>();
            prod.SetupGet(x => x.Name).Returns(name);
            prod.SetupGet(x => x.Price).Returns(price);
            prod.SetupGet(x => x.Discount).Returns(discount);

            var sut = new Item(prod.Object, 2);

            Assert.AreEqual(sut.Discount, 0, 0.0001);
        }

        [TestMethod]
        public void CheckRebate_ItemWithoutRebate()
        {
            var name = "Product1";
            var price = 10.0F;
            var discount = 0.01F;
            var prod = new Mock<IProduct>();
            prod.SetupGet(x => x.Name).Returns(name);
            prod.SetupGet(x => x.Price).Returns(price);
            prod.SetupGet(x => x.Discount).Returns(discount);
            prod.SetupGet(x => x.Discount).Returns(discount);

            var rebate = new Mock<IRebate>();
            rebate.SetupGet(x => x.Quantity).Returns(0);
            rebate.SetupGet(x => x.Percentage).Returns(0.5F);
            prod.SetupGet(x => x.Rebate).Returns(rebate.Object);

            var sut = new Item(prod.Object, 2);
           
            Assert.AreEqual(sut.Rebate, 0);
        }


        [TestMethod]
        public void CheckRebate_FullRebateOnFullQuantityWithExtraRebateItems()
        {
            var prod1 = new Mock<IProduct>();
            prod1.SetupGet(x => x.Name).Returns("Product1");
            prod1.SetupGet(x => x.Price).Returns(0.65F);
            prod1.SetupGet(x => x.Discount).Returns(0F);

            var prod2 = new Mock<IProduct>();
            prod2.SetupGet(x => x.Name).Returns("Product2");
            prod2.SetupGet(x => x.Price).Returns(0.80F);
            prod2.SetupGet(x => x.Discount).Returns(0F);

            var rebate = new Mock<IRebate>();
            rebate.SetupGet(x => x.Product).Returns(prod1.Object);
            rebate.SetupGet(x => x.Quantity).Returns(2);
            rebate.SetupGet(x => x.Percentage).Returns(1F);
            prod2.SetupGet(x => x.Rebate).Returns(rebate.Object);

            var sut = new Item(prod2.Object, 2);
            sut.LinkItem = new Item(prod1.Object, 5);

            Assert.AreEqual(sut.Rebate, 1.6, 0.0001);
        }


        [TestMethod]
        public void CheckRebate_HalfRebateOnFullQuantity()
        {
            var prod1 = new Mock<IProduct>();
            prod1.SetupGet(x => x.Name).Returns("Product1");
            prod1.SetupGet(x => x.Price).Returns(0.65F);
            prod1.SetupGet(x => x.Discount).Returns(0F);

            var prod2 = new Mock<IProduct>();
            prod2.SetupGet(x => x.Name).Returns("Product2");
            prod2.SetupGet(x => x.Price).Returns(0.80F);
            prod2.SetupGet(x => x.Discount).Returns(0F);

            var rebate = new Mock<IRebate>();
            rebate.SetupGet(x => x.Product).Returns(prod1.Object);
            rebate.SetupGet(x => x.Quantity).Returns(2);
            rebate.SetupGet(x => x.Percentage).Returns(0.5F);
            prod2.SetupGet(x => x.Rebate).Returns(rebate.Object);

            var sut = new Item(prod2.Object, 2);
            sut.LinkItem = new Item(prod1.Object, 4);

            Assert.AreEqual(sut.Rebate, 0.8, 0.0001);
        }

        [TestMethod]
        public void CheckRebate_HalfRebateOnFullQuantityWithDiscount()
        {
            var prod1 = new Mock<IProduct>();
            prod1.SetupGet(x => x.Name).Returns("Product1");
            prod1.SetupGet(x => x.Price).Returns(0.65F);
            prod1.SetupGet(x => x.Discount).Returns(0F);

            var prod2 = new Mock<IProduct>();
            prod2.SetupGet(x => x.Name).Returns("Product2");
            prod2.SetupGet(x => x.Price).Returns(0.80F);
            prod2.SetupGet(x => x.Discount).Returns(0.5F);

            var rebate = new Mock<IRebate>();
            rebate.SetupGet(x => x.Product).Returns(prod1.Object);
            rebate.SetupGet(x => x.Quantity).Returns(2);
            rebate.SetupGet(x => x.Percentage).Returns(0.5F);
            prod2.SetupGet(x => x.Rebate).Returns(rebate.Object);

            var sut = new Item(prod2.Object, 2);
            sut.LinkItem = new Item(prod1.Object, 4);
            var f = sut.Rebate;
            Assert.AreEqual(sut.Discount, 0.8, 0.0001);
            Assert.AreEqual(sut.Rebate, 0.4, 0.0001);
        }

        [TestMethod]
        public void CheckRebate_HalfRebateOnHalfQuantity()
        {
            var prod1 = new Mock<IProduct>();
            prod1.SetupGet(x => x.Name).Returns("Product1");
            prod1.SetupGet(x => x.Price).Returns(0.65F);
            prod1.SetupGet(x => x.Discount).Returns(0F);

            var prod2 = new Mock<IProduct>();
            prod2.SetupGet(x => x.Name).Returns("Product2");
            prod2.SetupGet(x => x.Price).Returns(0.80F);
            prod2.SetupGet(x => x.Discount).Returns(0F);

            var rebate = new Mock<IRebate>();
            rebate.SetupGet(x => x.Product).Returns(prod1.Object);
            rebate.SetupGet(x => x.Quantity).Returns(2);
            rebate.SetupGet(x => x.Percentage).Returns(0.5F);
            prod2.SetupGet(x => x.Rebate).Returns(rebate.Object);

            var sut = new Item(prod2.Object, 2);
            sut.LinkItem = new Item(prod1.Object, 2);
      
            Assert.AreEqual(sut.Rebate, 0.4, 0.0001);
        }
    }
}
