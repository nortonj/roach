﻿using BJSSTaskCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace BJSSTaskTest
{
    [TestClass]
    public class ShopTest
    {
        [TestMethod]
        public void PropertiesSet_OnConstruction()
        {
            var products = new Mock<IProducts>();
            var basket = new Mock<IBasket>();

            var sut = new Shop(products.Object, basket.Object);

            Assert.AreEqual(sut.Products, products.Object);
            Assert.AreEqual(sut.Basket, basket.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "Products not set")]
        public void AddToBasket_NullProducts()
        {
            var sut = new Shop(null, null);

            sut.AddToBasket("item");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Product1 not in products")]
        public void AddToBasket_NotInProducts()
        {
            var products = new Mock<IProducts>();
            products.SetupGet(x => x.List).Returns(new Dictionary<string, IProduct>());
            var basket = new Mock<IBasket>();
            var sut = new Shop(products.Object, basket.Object);

            sut.AddToBasket("Product1");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "Basket not set")]
        public void AddToBasket_NullBusket()
        {
            var sut = new Shop(null, null);

            sut.AddToBasket("item");
        }
    }
}
