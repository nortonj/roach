﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BJSSTaskCore;
using Moq;

namespace BJSSTaskTest
{
    [TestClass]
    public class BasketTest
    {
        [TestMethod]
        public void CheckAdd_OneItem()
        {
            var prod = new Mock<IProduct>();
            prod.Setup(x => x.Name).Returns("Product1");
            var item = new Mock<IItem>();
            item.Setup(x => x.Product).Returns(prod.Object);
            var sut = new Basket();

            sut.Add(item.Object);

            Assert.AreEqual(sut.List.Count, 1);
        }

        [TestMethod]
        public void CheckAdd_SameProductTwiceDiffItem()
        {
            var name = "Product";
            var prod = new Mock<IProduct>();
            prod.Setup(x => x.Name).Returns(name);

            int quantity1 = 2;
            var item1 = new Mock<IItem>();
            item1.Setup(x => x.Product).Returns(prod.Object);
            item1.SetupProperty(x => x.Quantity, quantity1);

            int quantity2 = 3;
            var item2 = new Mock<IItem>();
            item2.Setup(x => x.Product).Returns(prod.Object);
            item2.SetupProperty(x => x.Quantity, quantity2);

            var sut = new Basket();
            sut.Add(item1.Object);
            sut.Add(item2.Object);

            Assert.AreEqual(sut.List[name].Quantity, 5);
            Assert.AreEqual(sut.List.Count, 1);
        }

        [TestMethod]
        public void CheckAdd_DiffProductTwoItems()
        {
            var prod1 = new Mock<IProduct>();
            prod1.Setup(x => x.Name).Returns("Product1");

            var prod2 = new Mock<IProduct>();
            prod2.Setup(x => x.Name).Returns("Product2");

            int quantity1 = 2;
            var item1 = new Mock<IItem>();
            item1.Setup(x => x.Product).Returns(prod1.Object);
            item1.SetupProperty(x => x.Quantity, quantity1);

            int quantity2 = 3;
            var item2 = new Mock<IItem>();
            item2.Setup(x => x.Product).Returns(prod2.Object);
            item2.SetupProperty(x => x.Quantity, quantity2);

            var sut = new Basket();
            sut.Add(item1.Object);
            sut.Add(item2.Object);

            Assert.AreEqual(sut.List.Count, 2);
        }
    }
}
