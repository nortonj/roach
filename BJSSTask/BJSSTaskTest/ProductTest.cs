﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BJSSTaskCore;
using Moq;

namespace BJSSTaskTest
{
    [TestClass]
    public class ProductTest
    {
        [TestMethod]
        public void PropertiesSet_OnConstruction()
        {
            var name = "Soup";
            var price = 0.64F;
            var discount = 0.01F;
            var rebate = new Mock<IRebate>();

            var sut = new Product(name, price, discount);
            sut.Rebate = rebate.Object;

            Assert.AreEqual(sut.Name, name);
            Assert.AreEqual(sut.Price, price);
            Assert.AreEqual(sut.Discount, discount);
            Assert.AreEqual(sut.Rebate, rebate.Object);
        }
    }
}
