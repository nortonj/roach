﻿using System.Linq;
using BJSSTaskConsole.Startup;
using System;

namespace BJSSTaskConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var shop = Bootstrapper.Bootstrap();
            Console.WriteLine("\nEnter items eg PriceBasket Apples Apples Milk Bread Soup etc\n");
            string y = "y";

            while (y.ToLower().Equals("y"))
            {
                Console.WriteLine("\nEnter Basket");

                var input = Console.ReadLine().Split(' ');
                if (input.Length < 2 || !input[0].Equals("PriceBasket"))
                {
                    Console.WriteLine("\nIncorrect format eg PriceBasket Apples etc");
                }
                else
                {
                    shop.AddToBasket(input.Skip(1).ToArray());
                    shop.WriteBasketToConsole();
                }

                shop.EmptyBasket();
                Console.WriteLine("\nEnter 'Y' to continue, anyother key to escape.");

                y = Console.ReadLine();
            }
        }
    }
}
