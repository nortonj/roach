﻿using BJSSTaskCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace BJSSTaskConsole.Startup
{
    public static class Bootstrapper
    {
        public static IShop Bootstrap()
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(a => a.GetName().Name == "BJSSTaskCore");
            var catalog = new AssemblyCatalog(assembly);
            var container = new CompositionContainer(catalog);

            var shop = container.GetExportedValue<IShop>();

            return shop;
        }
    }
}
