﻿namespace BJSSTaskCore
{
    public class Product : IProduct
    {
        public string Name { get; private set; }
        public float Price { get; private set; }
        public float Discount { get; private set; }
        public IRebate Rebate { get; set; }

        public Product(string name, float price, float discount = 0)
        {
            Name = name;
            Price = price;
            Discount = discount;
        }
    }
}
