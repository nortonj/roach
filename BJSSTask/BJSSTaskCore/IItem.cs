﻿namespace BJSSTaskCore
{
    public interface IItem
    {
        IProduct Product { get; }
        float Cost { get; }
        float Discount { get; }
        float Rebate { get; }
        int Quantity { get; set; }
        IItem LinkItem { get; set; }

        void WriteToConsole(int dp=2);
    }
}