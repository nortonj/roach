﻿using System;

namespace BJSSTaskCore
{
    public static class FloatExtension
    {
        public static string ToMoneyString(this float value, int dp=2, char note='£', char coin='p')
        {
            if (value >= 1 || value <= -1)
                return $"{note}{value}";
            return $"{value * 100}{coin}";
        }

        public static float Round(this float value, int dp = 2)
        {
            return (float)Math.Round(value, dp);
        }
    }
}
