﻿using System.Collections.Generic;

namespace BJSSTaskCore
{
    public interface IProductProviderService
    {
        IDictionary<string, IProduct> List { get; set; }
    }
}