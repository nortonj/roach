﻿using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace BJSSTaskCore
{
    //alternative xml service, db service etc
    [Export(typeof(IProductProviderService))]
    public class DemoProductProviderService : IProductProviderService
    {
        public IDictionary<string, IProduct> List { get; set; } = new Dictionary<string, IProduct>();

        public DemoProductProviderService()
        {
            var soup = new Product("Soup", 0.65F);
            var bread = new Product("Bread", 0.80F);
            var milk = new Product("Milk", 1.30F);
            var apples = new Product("Apples", 1.00F, 0.1F);
            var breadRebate = new Rebate(soup, 2, 0.5F);
            bread.Rebate = breadRebate;

            List[soup.Name] = soup;
            List[bread.Name] = bread;
            List[milk.Name] = milk;
            List[apples.Name] = apples;
        }
    }
}
