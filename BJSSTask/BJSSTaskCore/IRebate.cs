﻿namespace BJSSTaskCore
{
    public interface IRebate
    {
        IProduct Product { get; }
        int Quantity { get; }
        float Percentage { get; }
    }
}