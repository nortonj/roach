﻿using System;
using System.ComponentModel.Composition;

namespace BJSSTaskCore
{
    [Export(typeof(IShop))]
    public class Shop : IShop
    {
        public IProducts Products { get; private set; }
        public IBasket Basket { get; private set; }

        [ImportingConstructor]
        public Shop([Import]IProducts products, [Import]IBasket basket)
        {
            Products = products;
            Basket = basket;
        }

        public void AddToBasket(string[] itemNames)
        {
            foreach(var item in itemNames)
                AddToBasket(item);
        }

        public void AddToBasket(string itemName, int quantity=1)
        {
            if (Products == null)
                throw new NullReferenceException("Products not set");
            if (Basket == null)
                throw new NullReferenceException("Basket not set");
            if (!Products.List.ContainsKey(itemName))
                throw new Exception($"{itemName} not in products");

            var item = new Item(Products.List[itemName], quantity);
            Basket.Add(item);
        }

        public void EmptyBasket()
        {
            Basket.Empty();
        }

        public void WriteBasketToConsole(int dp = 2)
        {
            Basket.WriteToConsole(dp);
        }
    }
}
