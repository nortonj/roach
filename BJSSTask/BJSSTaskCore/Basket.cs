﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace BJSSTaskCore
{
    [Export(typeof(IBasket))]
    public class Basket : IBasket
    {
        public IDictionary<string, IItem> List { get; private set; } = new Dictionary<string, IItem>();
        public float Cost { get { return List.Values.Select(i => i.Cost).Sum(); } }
        public float Discount { get { return List.Values.Select(i => i.Discount).Sum(); } }
        public float Rebate { get { return List.Values.Select(i => i.Rebate).Sum(); } }

        public void Add(IItem item)
        {
            if (List.ContainsKey(item.Product.Name))
            {
                List[item.Product.Name].Quantity += item.Quantity;
            }
            else
            {
                List[item.Product.Name] = item;
            }
        }

        public void SetRebateItems()
        {
            foreach (var item in List.Values)
            {
                var rebate = item.Product.Rebate;
                if (rebate != null)
                {
                    if (List.ContainsKey(rebate.Product.Name))
                    {
                        item.LinkItem = List[rebate.Product.Name];
                    }
                }
            }
        }

        public void WriteToConsole(int dp)
        {
            SetRebateItems();

            var cost = Cost.Round(dp);
            var discount = Discount.Round(dp);
            var rebate = Rebate.Round(dp);
            var total = cost - rebate - discount; 

            Console.WriteLine($"Subtotal: {cost.ToMoneyString()} ");

            if (discount + rebate == 0)
            {
                Console.WriteLine("(No offers available)");
            }
            else
            {
                foreach (var item in List.Values)
                {
                    item.WriteToConsole(dp);
                }
            }

            Console.WriteLine($"Total: {total.ToMoneyString()}");
        }

        public void Empty()
        {
            List.Clear();
        }
    }
}
