﻿namespace BJSSTaskCore
{
    public interface IProduct
    {
        float Discount { get; }
        string Name { get; }
        float Price { get; }
        IRebate Rebate { get; set; }
    }
}