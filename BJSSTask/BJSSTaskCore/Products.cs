﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace BJSSTaskCore
{
    [Export(typeof(IProducts))]
    public class Products : IProducts
    {
        public IDictionary<string, IProduct> List { get; private set; }

        [ImportingConstructor]
        public Products([Import]IProductProviderService productService)
        {
            List = productService.List;
        }

        public Products()
        {
            List  = new Dictionary<string, IProduct>();
        }

        public void Add(IProduct product)
        {
            if (List.ContainsKey(product.Name))
                throw new Exception($"{product.Name} already in products");
            List[product.Name] = product;
        }
    }
}
