﻿using System;

namespace BJSSTaskCore
{
    public class Item : IItem
    {
        public IProduct Product { get; private set;}
        public int Quantity { get; set; }
        public float Cost
        {
            get
            {
                return Quantity * Product.Price;
            }
        }
        public float Discount
        {
            get
            {
                return Cost * Product.Discount;
            }
        }
        public float Rebate
        {
            get
            {
                if (LinkItem == null || Product.Rebate == null || Product.Rebate.Quantity == 0 || Product.Rebate.Percentage == 0)
                {
                    return 0;
                }

                if (LinkItem.Quantity < Product.Rebate.Quantity)
                {
                    return 0;
                }

                var rebateQuantity = (LinkItem.Quantity - LinkItem.Quantity % Product.Rebate.Quantity) / Product.Rebate.Quantity;
                rebateQuantity = rebateQuantity > Quantity ? Quantity : rebateQuantity;

                var rebate = rebateQuantity * (Product.Price - Product.Price * Product.Discount) * Product.Rebate.Percentage;

                return rebate;
            }
        }
        public IItem LinkItem { get; set; }

        public Item(IProduct product, int quantity = 1)
        {
            Product = product;
            Quantity = quantity;
        }

        public void WriteToConsole(int dp)
        {
            if (Discount + Rebate > 0)
            {
                var discount = (-Discount-Rebate).Round(dp);
                var percentOff = Math.Round(-discount / Cost * 100, 0);
                Console.WriteLine($"{Product.Name} {percentOff}% off: {discount.ToMoneyString()}");
            }
        }
    }
}
