﻿namespace BJSSTaskCore
{
    public class Rebate : IRebate
    {
        public IProduct Product { get; private set; }
        public int Quantity { get; private set; }
        public float Percentage { get; private set; }

        public Rebate(IProduct product, int quantity, float percentageRebate)
        {
            Product = product;
            Quantity = quantity;
            Percentage = percentageRebate;
        }
    }
}
