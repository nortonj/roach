﻿using System.Collections.Generic;

namespace BJSSTaskCore
{
    public interface IBasket
    {
        IDictionary<string, IItem> List { get; }
        float Cost { get; }
        float Discount { get; }
        float Rebate { get; }

        void Add(IItem item);
        void SetRebateItems();
        void WriteToConsole(int dp);
        void Empty();
    }
}