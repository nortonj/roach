﻿namespace BJSSTaskCore
{
    public interface IShop
    {
        IBasket Basket { get; }
        IProducts Products { get; }

        void AddToBasket(string[] items);
        void AddToBasket(string name, int quantity = 1);
        void WriteBasketToConsole(int dp = 2);
        void EmptyBasket();
    }
}