﻿using System.Collections.Generic;

namespace BJSSTaskCore
{
    public interface IProducts
    {
        IDictionary<string, IProduct> List { get; }

        void Add(IProduct product);
    }
}