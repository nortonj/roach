﻿using System.Collections.Generic;
using System.Linq;

namespace ColossusCore
{
    public static class ColossusArrayExtensions
    {
        public static int[] LongestIncreasingSequence(this int[] sequence)
        {
            var increasing = new List<int>();
            var longest = new List<int>();

            var count = sequence.Length - 1;

            for (var i = 0; i <= count; i++)
            {
                if (i < count && sequence[i] < sequence[i + 1])
                {
                    increasing.Add(sequence[i]);
                }
                else
                {
                    increasing.Add(sequence[i]);

                    if (increasing.Count > longest.Count)
                    {
                        longest = increasing;
                    }

                    if (longest.Count >= count - i)
                    {
                        break;// not enought left to beat longest
                    }

                    increasing = new List<int>();
                }     
            }

            return longest.ToArray<int>();
        }

        public static bool ValueAndOrderEqual(this int[] array1, int[] array2)
        {
            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (var i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
