﻿using ColossusCore;
using System;
using System.Linq;

namespace ColossusConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\nEnter array of ints with spaces eg 1 3 4 2");
            string y = "y";

            while (y.ToLower().Equals("y"))
            {
                Console.Write("\nEnter: ");

                var input = Console.ReadLine().Trim(' ').Split(' ');
                var sequence = input.Select(x=> int.Parse(x)).ToArray();
                var longest = sequence.LongestIncreasingSequence();
                var result = String.Join(" ", longest);
                Console.WriteLine($"Longest: {result}");
                Console.WriteLine("\nEnter 'Y' to continue, anyother key to escape.");

                y = Console.ReadLine();
            }
        }
    }
}
