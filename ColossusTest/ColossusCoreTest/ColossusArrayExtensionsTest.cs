﻿using ColossusCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ColossusCoreTest
{
    [TestClass]
    public class ColossusArrayExtensionsTest
    {
        [TestMethod]
        public void LongestIncreasingSequence_FourWithFourIncreasing()
        {
            var sequence = new int[] { 1, 2, 3, 4 };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = sequence;

            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_FourWithThreeIncreasing()
        {
            var sequence = new int[] { 1, 1, 2, 3 };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] { 1, 2, 3 };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_ThreeThenFourIncreasing()
        {
            var sequence = new int[] { 5, 7, 10, 3, 6, 7, 9 };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] { 3, 6, 7, 9 };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_FourThenThreeIncreasing()
        {
            var sequence = new int[] { 3, 6, 7, 9, 5, 7, 10};
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] { 3, 6, 7, 9 };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_TwoThenFourThenThreeIncreasing()
        {
            var sequence = new int[] {4, 4, 3, 6, 7, 9, 5, 7, 10 };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] { 3, 6, 7, 9 };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_Empty()
        {
            var sequence = new int[] { };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] { };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void LongestIncreasingSequence_ThreeEqual()
        {
            var sequence = new int[] {2, 2, 2 };
            var longest = ColossusArrayExtensions.LongestIncreasingSequence(sequence);
            var expected = new int[] {2 };
            Assert.IsTrue(longest.ValueAndOrderEqual(expected));
        }

        [TestMethod]
        public void ValueAndOrderEqual_ThreeEqual()
        {
            var array1 = new int[] { 1, 5, 3 };
            var array2 = new int[] { 1, 5, 3 };
            var res = array1.ValueAndOrderEqual(array2);

            var res1 = array1.ValueAndOrderEqual(array2);
            var res2 = array2.ValueAndOrderEqual(array1);

            Assert.IsTrue(res1);
            Assert.IsTrue(res2);
        }

        [TestMethod]
        public void ValueAndOrderEqual_ThreeEqualButDifferentOrder()
        {
            var array1 = new int[] { 1, 5, 3 };
            var array2 = new int[] { 1, 3, 5 };
            var res1 = array1.ValueAndOrderEqual(array2);
            var res2 = array2.ValueAndOrderEqual(array1);

            Assert.IsTrue(!res1);
            Assert.IsTrue(!res2);
        }

        [TestMethod]
        public void ValueAndOrderEqual_AlmostEqual()
        {
            var array1 = new int[] { 1, 2, 7, 9 };
            var array2 = new int[] { 1, 2, 7, 5 };
            var res1 = array1.ValueAndOrderEqual(array2);
            var res2 = array2.ValueAndOrderEqual(array1);

            Assert.IsTrue(!res1);
            Assert.IsTrue(!res2);;
        }
    }
}
